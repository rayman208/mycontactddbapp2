package com.example.mycontactddbapp2;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    Button buttonAddContact, buttonClearAllContacts, buttonEditContact;
    EditText editTextName, editTextPhone, editTextNote;
    ListView listViewContacts;

    TableContacts tableContacts;
    Contact selectedContact;

    private void UpdateListViewContacts()
    {
        ContactsAdapter contactsAdapter = new ContactsAdapter(getApplicationContext(), tableContacts.GetAll());

        listViewContacts.setAdapter(contactsAdapter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editTextName = findViewById(R.id.editTextName);
        editTextPhone = findViewById(R.id.editTextPhone);
        editTextNote = findViewById(R.id.editTextNote);

        buttonAddContact = findViewById(R.id.buttonAddContact);
        buttonAddContact.setOnClickListener(buttonAddContactClickListener);

        buttonClearAllContacts = findViewById(R.id.buttonClearAllContacts);
        buttonClearAllContacts.setOnClickListener(buttonClearAllContactsClickListener);

        listViewContacts = findViewById(R.id.listViewContacts);

        SQLiteDatabase db = getBaseContext().openOrCreateDatabase("app.db",MODE_PRIVATE,null);
        tableContacts = new TableContacts(db);

        UpdateListViewContacts();

        registerForContextMenu(listViewContacts);
        selectedContact=null;

        buttonEditContact = findViewById(R.id.buttonEditContact);
        buttonEditContact.setVisibility(View.GONE);
        buttonEditContact.setOnClickListener(buttonEditContactClickListener);
    }

    View.OnClickListener buttonAddContactClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            String name = editTextName.getText().toString();
            String phone = editTextPhone.getText().toString();
            String note = editTextNote.getText().toString();

            if(name.equals(""))
            {
                Toast.makeText(getApplicationContext(),"Input Name!",Toast.LENGTH_LONG).show();
                return;
            }

            if(phone.equals(""))
            {
                Toast.makeText(getApplicationContext(),"Input Phone!",Toast.LENGTH_LONG).show();
                return;
            }

            tableContacts.AddNew(new Contact(0, name, phone, note));
            UpdateListViewContacts();
            //todo add new and update list view

            editTextName.getText().clear();
            editTextPhone.getText().clear();
            editTextNote.getText().clear();

            Toast.makeText(getApplicationContext(),"Contact added",Toast.LENGTH_LONG).show();
        }
    };

    View.OnClickListener buttonEditContactClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            String name = editTextName.getText().toString();
            String phone = editTextPhone.getText().toString();
            String note = editTextNote.getText().toString();

            if(name.equals(""))
            {
                Toast.makeText(getApplicationContext(),"Input Name!",Toast.LENGTH_LONG).show();
                return;
            }

            if(phone.equals(""))
            {
                Toast.makeText(getApplicationContext(),"Input Phone!",Toast.LENGTH_LONG).show();
                return;
            }

            Contact contact = new Contact(selectedContact.getId(),name, phone, note);
            tableContacts.UpdateById(contact);

            UpdateListViewContacts();
            //todo add new and update list view

            editTextName.getText().clear();
            editTextPhone.getText().clear();
            editTextNote.getText().clear();

            Toast.makeText(getApplicationContext(),"Contact edited",Toast.LENGTH_LONG).show();
            buttonEditContact.setVisibility(View.GONE);
            buttonAddContact.setVisibility(View.VISIBLE);
        }
    };

    View.OnClickListener buttonClearAllContactsClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            tableContacts.DeleteAll();

            Toast.makeText(getApplicationContext(),"Contacts deleted",Toast.LENGTH_LONG).show();

            UpdateListViewContacts();
           //todo update list view
        }
    };

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo)
    {
        super.onCreateContextMenu(menu, v, menuInfo);

        menu.add(0,0,0,"Edit contact");
        menu.add(0,1,0,"Delete contact");

        int clickPosition = ((AdapterView.AdapterContextMenuInfo)menuInfo).position;
        selectedContact = (Contact) ((ListView)v).getItemAtPosition(clickPosition);
    }

    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item)
    {
        switch (item.getItemId())
        {
            case 0:
                buttonEditContact.setVisibility(View.VISIBLE);
                buttonAddContact.setVisibility(View.GONE);

                editTextName.setText(selectedContact.getName());
                editTextPhone.setText(selectedContact.getNumber());
                editTextNote.setText(selectedContact.getNote());

                break;
            case 1:
                tableContacts.DeleteById(selectedContact.getId());

                UpdateListViewContacts();

                Toast.makeText(getApplicationContext(),"Contact deleted!",Toast.LENGTH_LONG).show();
                break;
        }

        return true;
    }
}
