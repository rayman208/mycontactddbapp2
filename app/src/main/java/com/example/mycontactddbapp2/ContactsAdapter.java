package com.example.mycontactddbapp2;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class ContactsAdapter extends BaseAdapter {

    private ArrayList<Contact> contacts;

    Context context;
    LayoutInflater layoutInflater;

    public ContactsAdapter(Context context, ArrayList<Contact> contacts)
    {
        this.context = context;
        this.contacts = contacts;

        layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return contacts.size();
    }

    @Override
    public Object getItem(int i) {
        return contacts.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = view;

        if (v == null) {
            v = layoutInflater.inflate(android.R.layout.simple_list_item_1, viewGroup, false);
        }

        Contact contact = contacts.get(i);

        TextView textView = v.findViewById(android.R.id.text1);
        textView.setText(contact.getName()+"\n"+contact.getNumber()+"\n"+contact.getNote());

        return v;
    }
}
