package com.example.mycontactddbapp2;

public class Contact
{
    private int Id;
    private String Name;
    private String Number;
    private String Note;

    public Contact(int Id, String Name, String Number, String Note)
    {
        this.Id = Id;
        this.Name = Name;
        this.Number = Number;
        this.Note = Note;
    }

    public int getId() {
        return Id;
    }

    public String getName() {
        return Name;
    }

    public String getNote() {
        return Note;
    }

    public String getNumber() {
        return Number;
    }
}
