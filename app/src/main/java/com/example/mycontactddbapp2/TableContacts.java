package com.example.mycontactddbapp2;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

public class TableContacts
{
    private SQLiteDatabase db;

    public TableContacts(SQLiteDatabase db)
    {
        this.db = db;

        db.execSQL("CREATE TABLE IF NOT EXISTS \"contacts\" (\n" +
                "\t\"id\"\tINTEGER NOT NULL,\n" +
                "\t\"name\"\tTEXT NOT NULL,\n" +
                "\t\"number\"\tTEXT NOT NULL,\n" +
                "\t\"note\"\tTEXT NOT NULL,\n" +
                "\tPRIMARY KEY(\"id\" AUTOINCREMENT)\n" +
                ")");
    }

    public ArrayList<Contact> GetAll()
    {
        ArrayList<Contact> contacts =new ArrayList<>();

        Cursor cursor = db.rawQuery("SELECT * FROM contacts", null);

        if(cursor.moveToFirst()==true)
        {
            do
            {
                int id = cursor.getInt(0);
                String name = cursor.getString(1);
                String phone = cursor.getString(2);
                String note = cursor.getString(3);

                contacts.add(new Contact(id,name,phone,note));

            }while (cursor.moveToNext()==true);
        }

        cursor.close();

        return contacts;
    }

    public void DeleteAll()
    {
        db.execSQL("DELETE FROM contacts");
    }

    public void DeleteById(int id)
    {
        db.execSQL("DELETE FROM contacts WHERE id="+id);
    }

    public void AddNew(Contact contact)
    {
        db.execSQL("INSERT INTO contacts (name, number, note) \n" +
                "VALUES ('"+contact.getName()+"','"+contact.getNumber()+"','"+contact.getNote()+"')");
    }

    public void UpdateById(Contact contact)
    {
        db.execSQL("UPDATE contacts SET name='"+contact.getName()+"', number='"+contact.getNumber()+"', note='"+contact.getNote()+"' WHERE id="+contact.getId());
    }

}
